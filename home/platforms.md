# [Platforms](#platforms)

## ARM Cortex-M

### [Tock]

[Tock]: https://www.tockos.org/

> Tock is a safe, multitasking operating system for low-power, low-memory
> microcontrollers.

Tock supports the following development boards:

- Hail
- Imix
- nRF51-DK

Check their [hardware] page.

[hardware]: https://www.tockos.org/hardware/

For kernel development check the [tock] repository.

[tock]: https://github.com/helena-project/tock

For userland applications check their WIP [libtock] repository.

[libtock]: https://github.com/helena-project/libtock-rs

### Real Time For the Masses

The Real Time For the Masses (RTFM) framework is a framework (not an OS) for
building efficient concurrent applications.

The [`cortex-m-rtfm`] crate is an implementation of the framework for ARM
Cortex-M microcontrollers. The crate fully supports Cortex M3, M4 and M7
microcontrollers and partially supports Cortex M0 and M0+ microcontrollers. The
RTFM framework only provides scheduling and synchronization primitives, and it's
totally decoupled from the I/O layer so you'll have to build your own I/O
abstractions or use one of the existing ones on crates.io. The only requirement
of the framework is a device crate generated using the [svd2rust] tool.

[`cortex-m-rtfm`]: https://crates.io/crates/cortex-m-rtfm
[svd2rust]: https://crates.io/crates/svd2rust

#### [STM32F3DISCOVERY](http://www.st.com/en/evaluation-tools/stm32f3discovery.html)

The [`f3`] crate provides support for this development board. Version [0.3.x]
provides a *blocking* API for LEDs, Serial, timers, the accelerometer, the
magnetometer, the gyroscope, etc. Version [0.4.x] provides a *non-blocking* API
compatible with the RTFM framework but it's not yet on parity with version
0.3.x.

[`f3`]: https://github.com/japaric/f3
[0.3.x]: https://docs.rs/f3/0.3.1/f3/
[0.4.x]: https://docs.rs/f3/0.4.1/f3/

The [Discovery] book targets this development board and makes use of the version
0.3.x.

You can use the 0.4.x version with the [`cortex-m-quickstart`] template.

[`cortex-m-quickstart`]: https://crates.io/crates/cortex-m-quickstart

#### [Blue Pill](http://wiki.stm32duino.com/index.php?title=Blue_Pill)

The [`blue-pill`] crate provides support for this development board. The crate
provides a non blocking API and it's compatible with the RTFM framework.

[`blue-pill`]: https://github.com/japaric/blue-pill

You can use this crate with the [`cortex-m-quickstart`] template.

#### [STM32VLDISCOVERY](http://www.st.com/en/evaluation-tools/stm32vldiscovery.html)

The [`vl`] crate provides support for this development board. The crate
provides a non blocking API and it's compatible with the RTFM framework.

[`vl`]: https://github.com/japaric/vl

You can use this crate with the [`cortex-m-quickstart`] template.

### [Particle Photon](https://store.particle.io/products/photon)

The [`photon-quickstart`] Cargo project is a template for building Photon
applications. The [`photon-hal`] crate provides a safe Rustic API on top of the
Photon HAL which is written in C. That HAL doesn't expose the whole Photon API
but already provides WiFi functionality for integration with the Particle cloud
API.

[`photon-quickstart`]: https://github.com/japaric/photon-quickstart
[`photon-hal`]: https://github.com/japaric/photon-hal

### [Teensy](https://www.pjrc.com/teensy/)

The [`teensy3`] crate provides a safe and rustic API on top of the Teensyduino
HAL which is written in C. The [`teensy3-rs-demo`] repository contains a Cargo
project template for using the `teensy3` crate. The [`teensy-clock`] repository
contains an example of building a wall clock using the `teensy3` crate.

[`teensy3`]: https://github.com/jamesmunns/teensy3-rs
[`teensy3-rs-demo`]: https://github.com/jamesmunns/teensy3-rs-demo
[`teensy-clock`]: https://github.com/SimonSapin/teensy-clock

### [Stellaris LM4F120 Launchpad](http://www.ti.com/tool/ek-lm4f120xl)

The [`lm4f120`] crate provides an API to use LM4F120 microcontrollers. The
[`stellaris-launchpad`] crate is a crate for working with the Stellaris
launchpad and at the same time it's a Cargo project template.

[`lm4f120`]: https://crates.io/crates/lm4f120
[`stellaris-launchpad`]: https://crates.io/crates/stellaris-launchpad

### [nRF51822][nrf51] (WIP)

[nrf51]: https://www.nordicsemi.com/Products/nRF51-Series-SoC

A Rust layer is being built on top of the SoftDevice firmware. The WIP
[`ble400`] crate provides support for the [BLE400] development board which has a
nRF51822 microcontroller, and makes use of the version S130 of the SoftDevice
firmware.

[`ble400`]: https://github.com/japaric/ble400
[BLE400]: http://www.waveshare.com/wiki/BLE400

### Other devices

You can easily bootstrap support for almost any ARM Cortex-M microntroller using
[svd2rust] and following the [quickstart] guide. Pretty much the only
requirement is having a SVD file for your device. You can probably find such
file in this [database] or on your microcontroller vendor's website.

[database]: https://github.com/posborne/cmsis-svd/tree/master/data

There are quite a few device crates that have been generated with svd2rust
floating around:

- [`nrf51`](https://crates.io/crates/nrf51)
- [`stm32f0xx`](https://crates.io/crates/stm32f0xx)
- [`stm32f100xx`](https://crates.io/crates/stm32f100xx)
- [`stm32f103xx`](https://crates.io/crates/stm32f103xx)
- [`stm32f30x`](https://crates.io/crates/stm32f30x)
- [`stm32f40x`](https://crates.io/crates/stm32f40x)
- [`stm32f429x`](https://crates.io/crates/stm32f429x)
- [`stm32l4x6`](https://crates.io/crates/stm32l4x6)

## MSP430 (WIP)

Support for the MSP430 architecture is being worked on. The corresponding LLVM
backend has been enabled in the rustc compiler, but there are no target
definition built into the compiler. However you can build applications starting
from [this template].

[this template]: https://github.com/pftbest/rust_on_msp

Progress is being tracked in [this issue][msp430].

[msp430]: https://github.com/rust-embedded/rfcs/issues/20

## AVR (WIP)

Support for the AVR architecture is being worked on [*out of tree*]. The
corresponding LLVM backend is *not* enabled in the rustc compiler so you'll need
to build a custom compiler to target this architecture. The backend has not been
enabled in rustc because it still has bugs and you can't compile the core crate,
which is required for every single Rust program out there, for the AVR
architecture.

[*out of tree*]: https://github.com/avr-rust

If you are willing to build a custom compiler and use a fork of the core crate
that woks with the LLVM AVR backend in its current state you can follow [this
blog post].

[this blog post]: https://gergo.erdi.hu/blog/2017-05-12-rust_on_avr__beyond_blinking/

## ARM Linux

### Raspberry Pi & co

The [rust-embedded] organization have been working on Rust support for Single
Board Computers like the Raspberry Pi. The organization maintains several
crates that are published on crates.io for doing embedded I/O on these boards.
Among those crates we have:

[rust-embedded]: https://github.com/rust-embedded

- [`gpio-utils`](https://crates.io/crates/gpio-utils). Command-line utilities
  for interacting with GPIOs under Linux.
- [`i2cdev`](https://crates.io/crates/i2cdev). Provides API for safe access to
  Linux i2c device interface.
- [`rust-sysfs-gpio`](https://crates.io/crates/sysfs-gpio). Provides access to
  the Linux sysfs interface to GPIOs.
- [`spidev`](https://crates.io/crates/spidev). Provides access to the Linux
  spidev interface.
- [`sysfs-pwm`](https://crates.io/crates/sysfs-pwm). Provides access to the
  Linux sysfs interfaces to PWMs.
