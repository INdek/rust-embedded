# [Resources](#resources)

## Getting started

- If you haven't worked with microcontrollers before the [Discovery] book will
  walk you through the basics.

[Discovery]: https://japaric.github.io/discovery/

- If you know the basics this [quickstart] blog post is the fastest way to get
  something working on pretty much any ARM Cortex-M microcontroller.

[quickstart]: http://blog.japaric.io/quickstart

- If you want to try out an embedded OS written in Rust try the [Tock OS getting
  started guide][tock-getting-started].

[tock-getting-started]: https://www.tockos.org/documentation/getting-started/

## Blogs

As we are still figuring things out people are writing down their experiences
with Rust and sharing information about how to develop embedded applications.
Keep an eye on these blogs:

- [Embedded in Rust]. A blog about developing pure Rust bare metal applications.

[Embedded in Rust]: http://blog.japaric.io

- [Talking Tock]. A blog about the development of the Tock OS.

[Talking Tock]: https://www.tockos.org/blog/

## Chat

The embedded Rust community gathers in the [#rust-embedded] IRC channel on
Mozilla's network (irc.mozilla.org).

[#rust-embedded]: https://bot.tockos.org/tockbot-moz/embedded-bot/
