# [Crates](#crates)

## Tools

- [Xargo] is a drop-in Cargo replacement that builds and manages sysroots. Xargo
  is required for no-std development where there are no binary releases of the
  core crate.

[Xargo]: https://crates.io/crates/xargo

- [Cross] is a drop-in Cargo replacement that facilitates cross compilation from
  x86_64 Linux to ARM / Aarch64 / MIPS / PowerPC Linux, *BSD, Windows, etc.

[Cross]: https://crates.io/crates/cross

- [`svd2rust`] is a tool for converting the System View Description (SVD) file
  of a microcontroller into a crate that exposes an API to use that
  microcontroller hardware.

[`svd2rust`]: https://crates.io/crates/svd2rust

- [`dslite2svd`] is a tool for converting TI's version of SVD files into
  standard SVD files that can be used with [svd2rust].

[`dslite2svd`]: https://github.com/m-labs/dslite2svd

- [`itmdump`] is a tool for decoding ITM frames and printing their payloads to
  stdout.

[`itmdump`]: https://crates.io/crates/itm

- [`bindgen`] a tool for automatically generating Rust FFI bindings to C and C++
  libraries.

[`bindgen`]: https://crates.io/crates/bindgen

## Formatting

- [`fast_fmt`]. Faster and leaner alternative to the `core::fmt` formatting
  machinery.

[`fast_fmt`]: https://crates.io/crates/fast_fmt

- [`numtoa`]. A crate for formatting numbers into byte arrays (`[u8]`)

[`numtoa`]: https://crates.io/crates/numtoa

## Memory management

- [`alloc-cortex-m`]. A dynamic memory allocator for Cortex-M microcontrollers.

[`alloc-cortex-m`]: https://crates.io/crates/alloc-cortex-m

- [`alloc_buddy_simple`]. Simple, drop-in replacement allocator for Rust running
  on bare metal (no_std)

[`alloc_buddy_simple`]: https://crates.io/crates/alloc_buddy_simple

- [`linked_list_allocator`]. Simple allocator usable for no_std systems. It
  builds a linked list from the freed blocks and thus needs no additional data
  structures.

[`linked_list_allocator`]: https://crates.io/crates/linked_list_allocator

## Data structures

- [`aligned`]. Statically allocated arrays with guaranteed memory alignments

[`aligned`]: https://crates.io/crates/aligned

- [`fixedvec`]. A heapless version of the Rust vector type.

[`fixedvec`]: https://crates.io/crates/fixedvec

- [`heapless`]. Data structures that can be placed in `static` variables.

[`heapless`]: https://crates.io/crates/heapless

- [`intrusive-collections`]. Intrusive collections for Rust (linked list and
  red-black tree)

[`intrusive-collections`]: https://crates.io/crates/intrusive-collections

## Miscellaneous

- [`cty`]. Type aliases to C types like c_int for use with bindgen

[`cty`]: https://crates.io/crates/cty

- [`lazy_static`]. A macro for declaring lazily evaluated statics in Rust.

[`lazy_static`]: https://crates.io/crates/lazy_static

- [`m`]. A C free / pure Rust mathematical library ("libm") for `no_std` code

[`m`]: https://crates.io/crates/m

## Even more

For more crates check the following categories on crates.io:

- [Embedded Development](https://crates.io/categories/embedded)
- [Hardware Support](https://crates.io/categories/hardware-support)
- [No Standard Library](https://crates.io/categories/no-std)
