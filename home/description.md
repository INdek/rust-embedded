The embedded space is vast and Rust support varies depending on the application
space and platform. However, we can roughly split the ecosystem in two: `std`
land and `no-std` land, where the standard library is not used or can't be used.

In `std` land we mainly have Single Board Computers like the Raspberry Pi and
routers running OpenWRT or similar. Development for these platforms is very
similar to native development, modulo the need for cross compilation, so the
tooling is in good shape. The ecosystem has crates for doing embedded I/O
through the Linux subsystem and crates for interfacing some sensors.

In `no-std` land we mainly have microcontrollers either running some OS or
operating bare metal (without an OS). Development is pretty different from
native development but the tooling is getting in good shape and the
development workflows are documented. There are crates in the ecosystem for
doing I/O at the lowest level (with registers), one framework for doing bare
metal concurrency and one embedded OS but [robotics] and [IoT-ish] applications
have been demoed. We are currently working towards higher level abstractions and
always growing platform support.

[robotics]: https://mobile.twitter.com/japaricious/status/845697935572656128
[IoT-ish]: https://mobile.twitter.com/japaricious/status/862711205084975104
